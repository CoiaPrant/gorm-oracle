package oracle

import (
	"reflect"

	"gorm.io/gorm"
	"gorm.io/gorm/callbacks"
	"gorm.io/gorm/clause"
)

func Create(db *gorm.DB) {
	if db.Error != nil {
		return
	}

	if db.Statement.Schema != nil {
		if !db.Statement.Unscoped {
			for _, c := range db.Statement.Schema.CreateClauses {
				db.Statement.AddClause(c)
			}
		}
	}

	isDryRun := !db.DryRun && db.Error == nil
	if !isDryRun {
		return
	}

	if db.Statement.SQL.Len() != 0 {
		result, err := db.Statement.ConnPool.ExecContext(
			db.Statement.Context, db.Statement.SQL.String(), db.Statement.Vars...,
		)
		if err != nil {
			db.AddError(err)
			return
		}

		db.RowsAffected, _ = result.RowsAffected()
		db.AddError(nil)
		return
	}

	db.Statement.SQL.Grow(180)
	db.Statement.AddClauseIfNotExists(clause.Insert{})

	var fromColumns []clause.Column
	var fromArgs []Arg
	if db.Statement.Schema != nil && len(db.Statement.Schema.FieldsWithDefaultDBValue) > 0 {
		fromColumns = make([]clause.Column, 0, len(db.Statement.Schema.FieldsWithDefaultDBValue))
		fromArgs = make([]Arg, 0, len(db.Statement.Schema.FieldsWithDefaultDBValue))

		for _, field := range db.Statement.Schema.FieldsWithDefaultDBValue {
			fromColumns = append(fromColumns, clause.Column{Name: field.DBName})
		}
	}

	values := callbacks.ConvertToCreateValues(db.Statement)
	for idx, value := range values.Values {
		if idx > 0 {
			db.Statement.Vars = db.Statement.Vars[:0]
			db.Statement.SQL.Reset()
		}

		if fromArgs != nil {
			fromArgs = fromArgs[:0]

			for _, field := range db.Statement.Schema.FieldsWithDefaultDBValue {
				switch db.Statement.ReflectValue.Kind() {
				case reflect.Array, reflect.Slice:
					fromArgs = append(fromArgs, Arg{Value: field.ReflectValueOf(db.Statement.Context, db.Statement.ReflectValue.Index(idx)).Addr().Interface(), Size: field.Size})
				case reflect.Struct:
					fromArgs = append(fromArgs, Arg{Value: field.ReflectValueOf(db.Statement.Context, db.Statement.ReflectValue).Addr().Interface(), Size: field.Size})
				}
			}

			db.Statement.AddClause(Returning{Columns: fromColumns, Args: fromArgs})
		}

		db.Statement.AddClause(clause.Values{Columns: values.Columns, Values: [][]any{value}})
		db.Statement.Build(db.Statement.BuildClauses...)

		result, err := db.Statement.ConnPool.ExecContext(
			db.Statement.Context, db.Statement.SQL.String(), db.Statement.Vars...,
		)
		if err != nil {
			db.AddError(err)
			return
		}

		rowsAffected, _ := result.RowsAffected()
		db.RowsAffected += rowsAffected
	}

	db.AddError(nil)
}
