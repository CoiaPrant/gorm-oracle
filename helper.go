package oracle

import (
	"gorm.io/gorm"
)

func hasReturning(tx *gorm.DB) bool {
	if c, ok := tx.Statement.Clauses["RETURNING"]; ok {
		returning, _ := c.Expression.(Returning)
		if len(returning.Columns) == 0 || (len(returning.Columns) != len(returning.Args)) {
			return false
		}
		return true
	}

	return false
}
