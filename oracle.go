package oracle

import (
	"database/sql"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"time"

	go_ora "github.com/sijms/go-ora/v2"

	"gorm.io/gorm"
	"gorm.io/gorm/callbacks"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/migrator"
	"gorm.io/gorm/schema"
)

type Config struct {
	DriverName        string
	DSN               string
	Conn              gorm.ConnPool
	DefaultStringSize uint
}

type Dialector struct {
	*Config
}

var (
	// CreateClauses create clauses
	CreateClauses = []string{"INSERT", "VALUES", "ON CONFLICT", "RETURNING INTO"}
	// QueryClauses query clauses
	QueryClauses = []string{}
	// UpdateClauses update clauses
	UpdateClauses = []string{"UPDATE", "SET", "WHERE", "ORDER BY", "LIMIT"}
	// DeleteClauses delete clauses
	DeleteClauses = []string{"DELETE", "FROM", "WHERE", "ORDER BY", "LIMIT"}
)

func Open(dsn string) gorm.Dialector {
	return &Dialector{Config: &Config{DSN: dsn}}
}

func New(config Config) gorm.Dialector {
	return &Dialector{Config: &config}
}

func BuildJDBC(user, password, connStr string, options map[string]string) string {
	return go_ora.BuildJDBC(user, password, connStr, options)
}

func BuildUrl(server string, port int, service, user, password string, options map[string]string) string {
	return go_ora.BuildUrl(server, port, service, user, password, options)
}

func (d Dialector) DummyTableName() string {
	return "DUAL"
}

func (d Dialector) Name() string {
	return "oracle"
}

func (d Dialector) Initialize(db *gorm.DB) (err error) {
	if d.DriverName == "" {
		d.DriverName = "oracle"
	}

	if d.Conn != nil {
		db.ConnPool = d.Conn
	} else {
		db.ConnPool, err = sql.Open(d.DriverName, d.DSN)
		if err != nil {
			return
		}
	}

	// register callbacks
	callbackConfig := &callbacks.Config{
		CreateClauses: CreateClauses,
		QueryClauses:  QueryClauses,
		UpdateClauses: UpdateClauses,
		DeleteClauses: DeleteClauses,
	}

	callbacks.RegisterDefaultCallbacks(db, callbackConfig)

	if err = db.Callback().Create().Replace("gorm:create", Create); err != nil {
		return
	}

	for k, v := range d.ClauseBuilders() {
		db.ClauseBuilders[k] = v
	}
	return
}

const (
	// ClauseLimit for clause.ClauseBuilder LIMIT key
	ClauseLimit = "LIMIT"
)

func (d Dialector) ClauseBuilders() map[string]clause.ClauseBuilder {
	return map[string]clause.ClauseBuilder{
		ClauseLimit: func(c clause.Clause, builder clause.Builder) {
			limit, ok := c.Expression.(clause.Limit)
			if !ok {
				c.Build(builder)
				return
			}

			if stmt, ok := builder.(*gorm.Statement); ok {
				if _, ok := stmt.Clauses["ORDER BY"]; !ok {
					s := stmt.Schema
					builder.WriteString("ORDER BY ")
					if s != nil && s.PrioritizedPrimaryField != nil {
						builder.WriteQuoted(s.PrioritizedPrimaryField.DBName)
						builder.WriteByte(' ')
					} else {
						builder.WriteString("(SELECT NULL FROM ")
						builder.WriteString(d.DummyTableName())
						builder.WriteString(")")
					}
				}
			}

			if offset := limit.Offset; offset > 0 {
				builder.WriteString(" OFFSET ")
				builder.WriteString(strconv.Itoa(offset))
				builder.WriteString(" ROWS")
			}

			if limit := limit.Limit; limit != nil && *limit >= 0 {
				builder.WriteString(" FETCH NEXT ")
				builder.WriteString(strconv.Itoa(*limit))
				builder.WriteString(" ROWS ONLY")
			}
		},
	}
}

func (d Dialector) DefaultValueOf(*schema.Field) clause.Expression {
	return clause.Expr{SQL: "VALUES (DEFAULT)"}
}

func (d Dialector) Migrator(db *gorm.DB) gorm.Migrator {
	return Migrator{
		Migrator: migrator.Migrator{
			Config: migrator.Config{
				DB:                          db,
				Dialector:                   d,
				CreateIndexAfterCreateTable: true,
			},
		},
		Dialector: d,
	}
}

func (d Dialector) BindVarTo(writer clause.Writer, stmt *gorm.Statement, v interface{}) {
	writer.WriteString(":")
	writer.WriteString(strconv.Itoa(len(stmt.Vars)))
}

func (d Dialector) QuoteTo(writer clause.Writer, str string) {
	writer.WriteByte('"')
	defer writer.WriteByte('"')
	writer.WriteString(str)
}

var numericPlaceholder = regexp.MustCompile(`:(\d+)`)

func (d Dialector) Explain(rawSQL string, vars ...interface{}) string {
	for i, v := range vars {
		switch v := v.(type) {
		case bool:
			if v {
				vars[i] = 1
			} else {
				vars[i] = 0
			}
		case time.Time:
			vars[i] = v.Format(time.DateTime)
		case sql.Out, *sql.Out,
			go_ora.Out, *go_ora.Out:
			vars[i] = fmt.Sprintf(":%d", i)
		}
	}

	return logger.ExplainSQL(rawSQL, numericPlaceholder, `'`, vars...)
}

func (dialector Dialector) DataTypeOf(field *schema.Field) string {
	switch field.DataType {
	case schema.Bool:
		return "number(1,0)"
	case schema.Int, schema.Uint:
		return dialector.getSchemaIntAndUnitType(field)
	case schema.Float:
		return dialector.getSchemaFloatType(field)
	case schema.String:
		return dialector.getSchemaStringType(field)
	case schema.Time:
		return "date"
	case schema.Bytes:
		return dialector.getSchemaBytesType(field)
	default:
		return dialector.getSchemaCustomType(field)
	}
}

func (dialector Dialector) getSchemaFloatType(field *schema.Field) string {
	if field.Precision > 0 || field.Scale > 0 {
		return fmt.Sprintf("number(%d, %d)", field.Precision, field.Scale)
	}

	return "float"
}

func (dialector Dialector) getSchemaStringType(field *schema.Field) string {
	size := field.Size
	if size == 0 {
		if dialector.DefaultStringSize > 0 {
			size = int(dialector.DefaultStringSize)
		} else {
			hasIndex := field.TagSettings["INDEX"] != "" || field.TagSettings["UNIQUE"] != ""
			// TEXT, GEOMETRY or JSON column can't have a default value
			if field.PrimaryKey || field.HasDefaultValue || hasIndex {
				size = 191 // utf8mb4
			}
		}
	}

	if size == 0 || size > 4000 {
		return "clob"
	}

	return fmt.Sprintf("varchar2(%d)", size)
}

func (dialector Dialector) getSchemaBytesType(field *schema.Field) string {
	if field.Size > 0 && field.Size <= 2000 {
		return fmt.Sprintf("raw(%d)", field.Size)
	}

	if field.Size > 65536 && field.Size <= int(math.MaxInt32) {
		return "long raw"
	}

	return "blob"
}

func (dialector Dialector) getSchemaIntAndUnitType(field *schema.Field) string {
	constraint := func(sqlType string) string {
		if field.AutoIncrement {
			sqlType += " GENERATED BY DEFAULT AS IDENTITY"
		}
		return sqlType
	}

	if field.Precision > 0 {
		return fmt.Sprintf("number(%d, 0)", field.Precision)
	}

	switch {
	case field.Size <= 8:
		return constraint("number(3,0)")
	case field.Size <= 16:
		return constraint("number(5,0)")
	case field.Size <= 32:
		return constraint("number(10,0)")
	default:
		return constraint("number")
	}
}

func (dialector Dialector) getSchemaCustomType(field *schema.Field) string {
	sqlType := string(field.DataType)

	if field.AutoIncrement {
		sqlType += " GENERATED BY DEFAULT AS IDENTITY"
	}

	return sqlType
}

func (dialector Dialector) SavePoint(tx *gorm.DB, name string) error {
	return tx.Exec("SAVEPOINT " + name).Error
}

func (dialector Dialector) RollbackTo(tx *gorm.DB, name string) error {
	return tx.Exec("ROLLBACK TO SAVEPOINT " + name).Error
}
