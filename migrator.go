package oracle

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/migrator"
	"gorm.io/gorm/schema"
)

var typeAliasMap = map[string][]string{
	"number":           {"int", "integer", "smallint", "decimal"},
	"int":              {"number"},
	"integer":          {"number"},
	"smallint":         {"number"},
	"decimal":          {"number"},
	"float":            {"double precision"},
	"double precision": {"float"},
}

type Migrator struct {
	migrator.Migrator
	Dialector
}

func (m Migrator) FullDataTypeOf(field *schema.Field) (expr clause.Expr) {
	expr.SQL = m.Migrator.DataTypeOf(field)

	if field.HasDefaultValue && (field.DefaultValueInterface != nil || field.DefaultValue != "") {
		if field.DefaultValueInterface != nil {
			defaultStmt := &gorm.Statement{Vars: []interface{}{field.DefaultValueInterface}}
			m.Dialector.BindVarTo(defaultStmt, defaultStmt, field.DefaultValueInterface)
			expr.SQL += " DEFAULT " + m.Dialector.Explain(defaultStmt.SQL.String(), field.DefaultValueInterface)
		} else if field.DefaultValue != "(-)" {
			expr.SQL += " DEFAULT " + field.DefaultValue
		}
	}

	if value, ok := field.TagSettings["COMMENT"]; ok {
		expr.SQL += " COMMENT " + m.Dialector.Explain("?", value)
	}

	return
}

func (m Migrator) CurrentDatabase() (name string) {
	m.DB.Raw(
		fmt.Sprintf(`SELECT ORA_DATABASE_NAME as "Current Database" FROM %s`, m.DummyTableName()),
	).Row().Scan(&name)
	return
}

func (m Migrator) CreateTable(values ...interface{}) (err error) {
	for _, value := range values {
		err = m.TryRemoveOnUpdate(value)
		if err != nil {
			return
		}

		m.RunWithValue(value, func(stmt *gorm.Statement) error {
			for _, field := range stmt.Schema.Fields {
				field.NotNull = false
			}
			return nil
		})
	}
	return m.Migrator.CreateTable(values...)
}

func (m Migrator) DropTable(values ...interface{}) error {
	values = m.ReorderModels(values, false)
	for i := len(values) - 1; i >= 0; i-- {
		value := values[i]
		tx := m.DB.Session(&gorm.Session{})
		if err := m.RunWithValue(value, func(stmt *gorm.Statement) error {
			return tx.Exec("DROP TABLE ? CASCADE CONSTRAINTS", clause.Table{Name: stmt.Table}).Error
		}); err != nil {
			return err
		}
	}
	return nil
}

func (m Migrator) HasTable(value interface{}) bool {
	var count int64
	m.RunWithValue(value, func(stmt *gorm.Statement) error {
		currentUser, table := m.CurrentSchema(stmt.Table)
		if currentUser == "" {
			return m.DB.Raw("SELECT COUNT(*) FROM USER_TABLES WHERE TABLE_NAME = ?", table).Row().Scan(&count)
		} else {
			return m.DB.Raw("SELECT COUNT(*) FROM ALL_TABLES WHERE OWNER = ? and TABLE_NAME = ?", currentUser, table).Row().Scan(&count)
		}
	})

	return count > 0
}

// ColumnTypes column types return columnTypes,error
func (m Migrator) ColumnTypes(value interface{}) ([]gorm.ColumnType, error) {
	columnTypes := make([]gorm.ColumnType, 0)
	err := m.RunWithValue(value, func(stmt *gorm.Statement) error {
		var (
			currentUser, table = m.CurrentSchema(stmt.Table)
			rows, err          = m.DB.Session(&gorm.Session{}).Table(table).Where("ROWNUM = ?", 1).Rows()
		)

		if err != nil {
			return err
		}

		rawColumnTypes, err := rows.ColumnTypes()
		if err != nil {
			return err
		}

		if err := rows.Close(); err != nil {
			return err
		}

		var primaryKeys, uniqueKeys []string
		{
			var columns *sql.Rows
			var rowErr error
			if currentUser == "" {
				columns, rowErr = m.DB.Table(table).Raw("SELECT a.COLUMN_NAME, b.CONSTRAINT_TYPE FROM USER_CONS_COLUMNS a, USER_CONSTRAINTS b WHERE a.TABLE_NAME = ? AND a.CONSTRAINT_NAME = b.CONSTRAINT_NAME", table).Rows()
			} else {
				columns, rowErr = m.DB.Table(table).Raw("SELECT a.COLUMN_NAME, b.CONSTRAINT_TYPE FROM ALL_CONS_COLUMNS a, ALL_CONSTRAINTS b WHERE a.OWNER = ? AND a.OWNER = b.OWNER AND a.TABLE_NAME = ? AND a.CONSTRAINT_NAME = b.CONSTRAINT_NAME", currentUser, table).Rows()
			}
			if rowErr != nil {
				return rowErr
			}
			defer columns.Close()

			for columns.Next() {
				var columeName, columeType sql.NullString
				if err := columns.Scan(&columeName, &columeType); err != nil {
					return err
				}

				if columeName.String == "" || columeType.String == "" {
					continue
				}

				switch columeType.String {
				case "P":
					primaryKeys = append(primaryKeys, columeName.String)
				case "U":
					uniqueKeys = append(primaryKeys, columeName.String)
				}
			}
		}

		var columns *sql.Rows
		var rowErr error
		if currentUser == "" {
			columns, rowErr = m.DB.Table(table).Raw("SELECT a.COLUMN_NAME, a.DATA_TYPE, a.IDENTITY_COLUMN, a.CHAR_LENGTH, a.DATA_PRECISION, a.DATA_SCALE, a.DATA_DEFAULT, b.COMMENTS FROM USER_TAB_COLUMNS a, USER_COL_COMMENTS b WHERE a.TABLE_NAME = ? AND a.TABLE_NAME = b.TABLE_NAME AND a.COLUMN_NAME = b.COLUMN_NAME", table).Rows()
		} else {
			columns, rowErr = m.DB.Table(table).Raw("SELECT a.COLUMN_NAME, a.DATA_TYPE, a.IDENTITY_COLUMN, a.CHAR_LENGTH, a.DATA_PRECISION, a.DATA_SCALE, a.DATA_DEFAULT, b.COMMENTS FROM ALL_TAB_COLUMNS a, ALL_COL_COMMENTS b WHERE a.OWNER = ? AND a.TABLE_NAME = ? AND a.OWNER = b.OWNER AND a.TABLE_NAME = b.TABLE_NAME AND a.COLUMN_NAME = b.COLUMN_NAME", currentUser, table).Rows()
		}

		if rowErr != nil {
			return rowErr
		}
		defer columns.Close()

		for columns.Next() {
			var (
				column        migrator.ColumnType
				autoIncrement sql.NullString
				values        = []interface{}{
					&column.NameValue, &column.DataTypeValue, &autoIncrement, &column.LengthValue, &column.DecimalSizeValue, &column.ScaleValue, &column.DefaultValueValue, &column.CommentValue,
				}
			)

			if scanErr := columns.Scan(values...); scanErr != nil {
				return scanErr
			}

			if field := stmt.Schema.FieldsByDBName[column.NameValue.String]; field != nil {
				column.NullableValue = sql.NullBool{Bool: !field.NotNull, Valid: true}

				switch field.DataType {
				case schema.Bool:
					column.DecimalSizeValue = sql.NullInt64{Int64: int64(field.Precision), Valid: true}

					if column.DefaultValueValue.Valid {
						columnDefault, e1 := strconv.ParseBool(column.DefaultValueValue.String)

						var (
							fieldDefault bool
							e2           error
						)
						if field.HasDefaultValue {
							fieldDefault, e2 = strconv.ParseBool(field.DefaultValue)
						}
						if e1 == nil && e2 == nil && columnDefault == fieldDefault {
							column.DefaultValueValue = sql.NullString{String: field.DefaultValue, Valid: field.HasDefaultValue}
						}
					}

				case schema.Int, schema.Uint:
					if field.Precision == 0 {
						column.DecimalSizeValue = sql.NullInt64{Int64: int64(field.Precision), Valid: true}
					}

					if column.DefaultValueValue.Valid {
						columnDefault, e1 := strconv.ParseInt(column.DefaultValueValue.String, 10, 64)

						var (
							fieldDefault int64
							e2           error
						)
						if field.HasDefaultValue {
							fieldDefault, e2 = strconv.ParseInt(field.DefaultValue, 10, 64)
						}
						if e1 == nil && e2 == nil && columnDefault == fieldDefault {
							column.DefaultValueValue = sql.NullString{String: field.DefaultValue, Valid: field.HasDefaultValue}
						}
					}
				case schema.Float:
					if column.DefaultValueValue.Valid {
						columnDefault, e1 := strconv.ParseFloat(column.DefaultValueValue.String, 64)

						var (
							fieldDefault float64
							e2           error
						)
						if field.HasDefaultValue {
							fieldDefault, e2 = strconv.ParseFloat(field.DefaultValue, 64)
						}
						if e1 == nil && e2 == nil && columnDefault == fieldDefault {
							column.DefaultValueValue = sql.NullString{String: field.DefaultValue, Valid: field.HasDefaultValue}
						}
					}
				case schema.String:
					if column.DefaultValueValue.Valid {
						if column.DefaultValueValue.String == "''" {
							column.DefaultValueValue = sql.NullString{String: "", Valid: field.HasDefaultValue}
						}
					}
				case schema.Time:
				case schema.Bytes:
				default:
					if strings.HasPrefix(string(field.DataType), "decimal") {
						if column.DefaultValueValue.Valid {
							columnDefault, e1 := strconv.ParseFloat(column.DefaultValueValue.String, 64)

							var (
								fieldDefault float64
								e2           error
							)
							if field.HasDefaultValue {
								fieldDefault, e2 = strconv.ParseFloat(field.DefaultValue, 64)
							}
							if e1 == nil && e2 == nil && columnDefault == fieldDefault {
								column.DefaultValueValue = sql.NullString{String: field.DefaultValue, Valid: field.HasDefaultValue}
							}
						}
					}
				}
			}

			column.AutoIncrementValue = sql.NullBool{Bool: autoIncrement.String == "YES", Valid: autoIncrement.Valid}
			column.PrimaryKeyValue = sql.NullBool{Valid: true}
			for _, name := range primaryKeys {
				if name == column.NameValue.String {
					column.PrimaryKeyValue.Bool = true
					break
				}
			}

			column.UniqueValue = sql.NullBool{Valid: true}
			for _, name := range uniqueKeys {
				if name == column.NameValue.String {
					column.UniqueValue.Bool = true
					break
				}
			}

			for _, c := range rawColumnTypes {
				if c.Name() == column.NameValue.String {
					column.SQLColumnType = c
					break
				}
			}

			columnTypes = append(columnTypes, column)
		}

		return nil
	})
	return columnTypes, err
}

func (m Migrator) RenameTable(oldName, newName interface{}) (err error) {
	resolveTable := func(name interface{}) (result string, err error) {
		if v, ok := name.(string); ok {
			result = v
		} else {
			stmt := &gorm.Statement{DB: m.DB}
			if err = stmt.Parse(name); err == nil {
				result = stmt.Table
			}
		}
		return
	}

	var oldTable, newTable string

	if oldTable, err = resolveTable(oldName); err != nil {
		return
	}

	if newTable, err = resolveTable(newName); err != nil {
		return
	}

	return m.DB.Exec("RENAME TABLE ? TO ?",
		clause.Table{Name: oldTable},
		clause.Table{Name: newTable},
	).Error
}

func (m Migrator) AlterColumn(value interface{}, field string) error {
	return m.RunWithValue(value, func(stmt *gorm.Statement) error {
		if field := stmt.Schema.LookUpField(field); field != nil {
			fileType := m.FullDataTypeOf(field)
			return m.DB.Exec(
				"ALTER TABLE ? MODIFY ? ?",
				m.CurrentTable(stmt), clause.Column{Name: field.DBName}, fileType,
			).Error
		}
		return fmt.Errorf("failed to look up field with name: %s", field)
	})
}

func (m Migrator) HasColumn(value interface{}, field string) bool {
	var count int64
	m.RunWithValue(value, func(stmt *gorm.Statement) error {
		currentUser, table := m.CurrentSchema(stmt.Table)
		if currentUser == "" {
			return m.DB.Raw("SELECT COUNT(*) FROM USER_TAB_COLUMNS WHERE TABLE_NAME = ? AND COLUMN_NAME = ?", table, field).Row().Scan(&count)
		} else {
			return m.DB.Raw("SELECT COUNT(*) FROM ALL_TAB_COLUMNS WHERE OWNER = ? and TABLE_NAME = ? AND COLUMN_NAME = ?", currentUser, table, field).Row().Scan(&count)
		}
	})

	return count > 0
}

func (m Migrator) CreateConstraint(value interface{}, name string) error {
	m.TryRemoveOnUpdate(value)
	return m.Migrator.CreateConstraint(value, name)
}

func (m Migrator) DropConstraint(value interface{}, name string) error {
	return m.RunWithValue(value, func(stmt *gorm.Statement) error {
		for _, chk := range stmt.Schema.ParseCheckConstraints() {
			if chk.Name == name {
				return m.DB.Exec(
					"ALTER TABLE ? DROP CHECK ?",
					m.CurrentTable(stmt), clause.Column{Name: name},
				).Error
			}
		}

		return m.DB.Exec(
			"ALTER TABLE ? DROP CONSTRAINT ?",
			m.CurrentTable(stmt), clause.Column{Name: name},
		).Error
	})
}

func (m Migrator) HasConstraint(value interface{}, name string) bool {
	var count int64
	m.RunWithValue(value, func(stmt *gorm.Statement) error {
		return m.DB.Raw(
			"SELECT COUNT(*) FROM USER_CONSTRAINTS WHERE TABLE_NAME = ? AND CONSTRAINT_NAME = ?", stmt.Table, name,
		).Row().Scan(&count)
	})

	return count > 0
}

func (m Migrator) DropIndex(value interface{}, name string) error {
	return m.RunWithValue(value, func(stmt *gorm.Statement) error {
		return m.DB.Exec("DROP INDEX ?", name).Error
	})
}

func (m Migrator) HasIndex(value interface{}, name string) bool {
	var count int64
	m.RunWithValue(value, func(stmt *gorm.Statement) error {
		return m.DB.Raw(
			"SELECT COUNT(*) FROM USER_INDEXES WHERE TABLE_NAME = ? AND INDEX_NAME = ?", stmt.Table, name,
		).Row().Scan(&count)
	})

	return count > 0
}

func (m Migrator) RenameIndex(value interface{}, oldName, newName string) error {
	return m.RunWithValue(value, func(stmt *gorm.Statement) error {
		return m.DB.Exec(
			"ALTER INDEX ?.? RENAME TO ?",
			clause.Table{Name: stmt.Table}, clause.Column{Name: oldName}, clause.Column{Name: newName},
		).Error
	})
}

func (m Migrator) TryRemoveOnUpdate(values ...interface{}) error {
	for _, value := range values {
		if err := m.RunWithValue(value, func(stmt *gorm.Statement) error {
			for _, rel := range stmt.Schema.Relationships.Relations {
				constraint := rel.ParseConstraint()
				if constraint != nil {
					rel.Field.TagSettings["CONSTRAINT"] = strings.ReplaceAll(rel.Field.TagSettings["CONSTRAINT"], fmt.Sprintf("ON UPDATE %s", constraint.OnUpdate), "")
				}
			}
			return nil
		}); err != nil {
			return err
		}
	}
	return nil
}

func (m Migrator) CurrentSchema(table string) (string, string) {
	if tables := strings.Split(table, `.`); len(tables) == 2 {
		return tables[0], tables[1]
	}
	m.DB = m.DB.Table(table)
	return "", table
}

func (m Migrator) GetTypeAliases(databaseTypeName string) []string {
	return typeAliasMap[databaseTypeName]
}
