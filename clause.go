package oracle

import (
	go_ora "github.com/sijms/go-ora/v2"
	"gorm.io/gorm/clause"
)

type Arg struct {
	Value any
	Size  int
}

type Returning struct {
	Columns []clause.Column
	Args    []Arg
}

// Name where clause name
func (returning Returning) Name() string {
	return "RETURNING INTO"
}

// Build build where clause
func (returning Returning) Build(builder clause.Builder) {
	if len(returning.Columns) > 0 && len(returning.Columns) == len(returning.Args) {
		builder.WriteString("RETURNING")
		builder.WriteString(" ")

		for idx, column := range returning.Columns {
			if idx > 0 {
				builder.WriteByte(',')
			}

			builder.WriteQuoted(column)
		}

		builder.WriteString(" INTO ")

		for idx, arg := range returning.Args {
			if idx > 0 {
				builder.WriteByte(',')
			}

			builder.AddVar(builder, go_ora.Out{Dest: arg.Value, Size: arg.Size})
		}
	}
}

// MergeClause merge order by clauses
func (returning Returning) MergeClause(clause *clause.Clause) {
	if v, ok := clause.Expression.(Returning); ok {
		returning.Columns = append(v.Columns, returning.Columns...)
		returning.Args = append(v.Args, returning.Args...)
	}

	clause.Name = ""
	clause.Expression = returning
}
